release:
	rm -rf dist/
	npx webpack --mode production

debug:
	node index.js

install-dependencies:
	npm install
	npm install --save-dev webpack webpack-cli copy-webpack-plugin zip-webpack-plugin

clean:
	rm -rf dist/
	rm -rf node_modules/

.PHONY=release debug install-dependencies clean

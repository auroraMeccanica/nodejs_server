var express = require('express');
var app = express();

function main()
{
  // Serve from public folder inside the sd card
  app.use(express.static('/storage/sd/public'));

  // Serve from public folder inside the SSD drive
  // app.use(express.static('/storage/ssd/public'));

  // Serve from public folder inside a USB drive
  // app.use(express.static('/storage/usb1/public'));

  app.listen(9090, function()
    {
      console.log('Example app listening on port 9090!');
    }
  );
}

// exchange below line to 'window.main = main;' if using node-server.html
// as the application entry point.
main();

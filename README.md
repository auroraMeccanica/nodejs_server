# NodeJS server for brightsign units

When making brightsign units display webpages, it's useful to serve the pages from an internal localhost server instead of an external one.

In this way:
- there's no need for an internet connection
- faster loading time

Although HTML state in Brightsign players can show pages from local filesytem, sometimes it's mandatory to use a local web server:
- some features might be disabled when loading something from local file system;
- the tools you're using builds all URLs so they start with `/` instead of `./`. It's easier to serve them instead of having to tweak the url generation tool;

## Makefile targets

The project includes a `Makefile`.
Here are the available commands:
- `make install-dependencies`: installs all the nodejs modules to run the project and also the webpack utility to generate the js file to be distributed;
- `make release`, or simply `make`: generated the output files, compacts them with webpack and puts them inside the `dist` folder;
- `debug`: simply run the server on your development computer;
- `clean`: removes all output, temporary and nodejs modules folders, freeing up diskspace.


## Configuration

This project serves files from the `public` folder inside the sd card.
Edit the `index.js` to change where they're served from.

The drive paths are listed in the [Brightsign documentation](https://brightsign.atlassian.net/wiki/spaces/DOC/pages/370677153/Node.js#Device-Storage-Paths):
- microSD: `/storage/sd/`
- SSD: `/storage/ssd/`
- USB: `/storage/usb1/`
